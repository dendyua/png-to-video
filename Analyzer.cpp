
#include "Analyzer.h"

#include <memory>

#include <QThreadPool>
#include <QImage>
#include <QDebug>
#include <QFile>




void AnalyzeWorker::run() noexcept
{
	const QImage image(path_);
	if (image.isNull()) {
		emit error(QLatin1String("Cannot open image"));
		return;
	}

	const QSize imageSize = image.size();

	int l = imageSize.width() - 1;
	int r = 0;
	int t = imageSize.height() - 1;
	int b = 0;
	bool transparent = true;

	const int intAlphaThreshold = qRound(alphaThreshold_ * 255.0f);

	for (int y = 0; y < imageSize.height(); ++y) {
		const QRgb * const s = reinterpret_cast<const QRgb*>(image.scanLine(y));
		for (int x = 0; x < imageSize.width(); ++x) {
			const int a = qAlpha(s[x]);
			if (a <= intAlphaThreshold) continue;
			transparent = false;
			l = std::min(l, x);
			r = std::max(r, x);
			t = std::min(t, y);
			b = std::max(b, y);
		}
	}

	const QRect crop = transparent ? QRect() : QRect(QPoint(l, t), QPoint(r, b));

	emit finished(index_, imageSize, crop);
}




QSize Analyzer::findImageSize(const Path & path) noexcept
{
	const QImage image = QImage(path.pathForIndex(0));
	if (image.isNull()) return QSize();
	return image.size();
}


Analyzer::Analyzer(QThreadPool & pool, const Path & path, const int count,
		const float alphaThreshold) noexcept :
	pool_(pool),
	path_(path),
	count_(count),
	alphaThreshold_(alphaThreshold)
{
}


void Analyzer::start() noexcept
{
	while (pool_.activeThreadCount() < pool_.maxThreadCount()) {
		if (!_tryNext()) {
			_checkDone();
			break;
		}
	}
}


bool Analyzer::_tryNext() noexcept
{
	if (currentIndex_ == count_) return false;

	const int index = currentIndex_;
	const QString filePath = path_.pathForIndex(currentIndex_);

	currentIndex_++;

	std::unique_ptr<AnalyzeWorker> worker(new AnalyzeWorker(index, filePath, alphaThreshold_));

	QObject::connect(worker.get(), &QObject::destroyed, this, [this]() {
		workerCount_--;
		_checkDone();
	}, Qt::QueuedConnection);

	QObject::connect(worker.get(), &AnalyzeWorker::error, [this](const QString & message) {
		qDebug() << "error:" << message;
		hasError_ = true;
	});

	QObject::connect(worker.get(), &AnalyzeWorker::finished, this, [this](
			const int index, const QSize & size, const QRect & crop) {
		qInfo() << "analyzed:" << index
				<< "size:" << size << "crop:" << crop;

		if (!hasError_) {
			if (!size_.isValid()) {
				size_ = size;
			} else {
				if (size_ != size) {
					qWarning() << "Size does not match, expected:" << size_ << "got:" << size;
					hasError_ = true;
				}
			}
		}

		if (!hasError_) {
			if (crop_.isNull()) {
				crop_ = crop;
			} else {
				crop_ = crop_.united(crop);
			}

			_tryNext();
		}
	}, Qt::QueuedConnection);

	workerCount_++;

	pool_.start(worker.release());

	return true;
}


void Analyzer::_checkDone() noexcept
{
	if (workerCount_ != 0) {
		// waiting for other to complete
		return;
	}

	if (hasError_) {
		emit finished(false);
		return;
	}

	emit finished(true);
}
