
Run with --help to get command line options.

Output images can be converted into video with, for example, ffmpeg:

  $ ffmpeg -framerate 60 -start_number 0 -i myimages-%05d.png -c:v libx264 -r 60 -pix_fmt yuv420p myvideo.mp4

Where:
  myimages-%05d.png - format for output images produced by tool.
  myvideo.mp4       - output video file
