
#pragma once

#include <memory>

#include <QObject>
#include <QRunnable>
#include <QDir>
#include <QRect>
#include <QVector>
#include <QThreadPool>
#include <QColor>

#include "Common.h"
#include "Analyzer.h"
#include "Converter.h"




class App : public QObject
{
	Q_OBJECT

public:
	App() noexcept;

	int exec() noexcept;

private:
	void _parseArguments() noexcept(false);
	void _countFiles() noexcept;
	void _start() noexcept;
	void _startConvertor() noexcept;

	void _analyzerFinished(bool ok) noexcept;
	void _converterFinished(bool ok) noexcept;

private:
	QThreadPool threadPool_;

	Mode mode_ = Mode::Video;
	std::unique_ptr<Path> inputPath_;
	std::unique_ptr<Path> outputPath_;
	bool reverse_ = false;
	bool analyzeOnly_ = false;
	QRect exactCrop_;
	bool centerAutoCrop_ = false;
	int cropAdjust_ = 4;
	CropMode cropMode_ = CropMode::Auto;
	float scale_ = 0;
	int fileCount_;
	float brightness_ = 0;
	QColor fillColor_;
	float alphaThreshold_ = 0;

	std::unique_ptr<Analyzer> analyzer_;
	std::unique_ptr<Converter> converter_;

	QSize imageSize_;
	QRect imageCrop_;
};
