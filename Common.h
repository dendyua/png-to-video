
#pragma once

#include <QString>




enum class Mode {
	Video,
	Crop
};




enum class CropMode {
	None,
	Exact,
	Auto
};




class Path
{
public:
	Path(const QString & format, int length, int first) noexcept;

	QString pathForIndex(int index) const noexcept;

private:
	const QString format_;
	const int length_;
	const int first_;
};




inline Path::Path(const QString & format, const int length, const int first) noexcept :
	format_(format),
	length_(length),
	first_(first)
{}

inline QString Path::pathForIndex(int index) const noexcept
{ return format_.arg(first_ + index, length_, 10, QLatin1Char('0')); }
