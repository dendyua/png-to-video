
#pragma once

#include <QObject>
#include <QRunnable>
#include <QRect>
#include <QColor>

#include "Common.h"




class QThreadPool;




struct ConvertOptions {
	const Mode mode;
	const Path & inputPath;
	const Path & outputPath;
	const QRect crop;
	const QSize scaleSize;
	const float brightness;
	const QColor fillColor;
};




class ConverterWorker : public QObject, public QRunnable
{
	Q_OBJECT

public:
	ConverterWorker(const int index, const ConvertOptions & options,
			const QString & outputFilePath) noexcept :
		index_(index),
		options_(options),
		outputFilePath_(outputFilePath)
	{}

	void run() noexcept override;

signals:
	void error();
	void finished(int index);

private:
	const int index_;
	const ConvertOptions & options_;
	const QString outputFilePath_;
};




class Converter : public QObject
{
	Q_OBJECT

public:
	Converter(QThreadPool & pool, Mode mode, const Path & inputPath, const Path & outputPath,
			int count, const QRect & crop, bool reverse, const QSize & scaleSize,
			float brightness, const QColor & fillColor) noexcept;

	void start() noexcept;

signals:
	void finished(bool ok);

private:
	bool _tryNext() noexcept;
	void _checkDone() noexcept;

private:
	QThreadPool & pool_;
	const int count_;
	const bool reverse_;

	const ConvertOptions options_;

	int currentIndex_ = 0;
	int workerCount_ = 0;
	bool hasError_ = false;
	bool atEnd_ = false;
};
