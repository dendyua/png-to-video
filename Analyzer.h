
#pragma once

#include <QObject>
#include <QRunnable>
#include <QRect>

#include "Common.h"




class QThreadPool;




class AnalyzeWorker : public QObject, public QRunnable
{
	Q_OBJECT

public:
	AnalyzeWorker(int index, const QString & path, float alphaThreshold) noexcept :
		index_(index),
		path_(path),
		alphaThreshold_(alphaThreshold)
	{}

	const QString & path() const noexcept { return path_; }

	void run() noexcept override;

signals:
	void error(const QString & message);
	void finished(int index, const QSize & size, const QRect & crop);

private:
	const int index_;
	const QString path_;
	const float alphaThreshold_;
};




class Analyzer : public QObject
{
	Q_OBJECT

public:
	static QSize findImageSize(const Path & path) noexcept;

	Analyzer(QThreadPool & pool, const Path & path, int count, float alphaThreshold) noexcept;

	void start() noexcept;

	QRect crop() const noexcept { return crop_; }
	QSize size() const noexcept { return size_; }

signals:
	void finished(bool ok);

private:
	bool _tryNext() noexcept;
	void _checkDone() noexcept;

private:
	QThreadPool & pool_;
	const Path & path_;
	const int count_;
	const float alphaThreshold_;

	QRect crop_;
	QSize size_;

	int currentIndex_ = 0;
	int workerCount_ = 0;
	bool hasError_ = false;
};
