
#include "App.h"

#include <exception>
#include <iostream>
#include <memory>

#include <QCommandLineParser>
#include <QFileInfo>
#include <QThreadPool>
#include <QDebug>
#include <QTimer>




class ParserError : public std::exception
{
public:
	ParserError(const QCommandLineParser & parser, const QString & message = QString()) :
		error(parser.errorText()),
		usage(parser.helpText()),
		message(message)
	{}

	const QString error;
	const QString usage;
	const QString message;
};




App::App() noexcept
{
}


int App::exec() noexcept
{
	try {
		_parseArguments();
	} catch (const ParserError & e) {
		if (!e.message.isNull()) {
			std::cout << qPrintable(e.message) << std::endl << std::endl;
		}
		if (!e.error.isNull()) {
			std::cout << qPrintable(e.error) << std::endl << std::endl;
		}
		std::cout << qPrintable(e.usage);
		return 1;
	}

	std::unique_ptr<QTimer> startTimer(new QTimer);
	startTimer->setInterval(0);
	QTimer * const startTimerPointer = startTimer.get();
	connect(startTimer.get(), &QTimer::timeout, [this, startTimerPointer]() {
		std::unique_ptr<QTimer> startTimer(startTimerPointer);
		Q_UNUSED(startTimer)
		_start();
	});
	startTimer.release()->start();

	return QCoreApplication::exec();
}


void App::_parseArguments() noexcept(false)
{
	const struct Option {
		QString mode        = QLatin1String("mode");
		QString reverse     = QLatin1String("r");
		QString analyzeOnly = QLatin1String("a");
		QString crop        = QLatin1String("crop");
		QString cropAdjust  = QLatin1String("crop-adjust");
		QString scale       = QLatin1String("scale");
		QString brightness  = QLatin1String("b");
		QString fill        = QLatin1String("fill");
		QString alpha       = QLatin1String("alpha");

		// mode values
		QString modeVideo = QLatin1String("video");
		QString modeCrop  = QLatin1String("crop");

		// crop values
		QString cropNone   = QLatin1String("none");
		QString cropAuto   = QLatin1String("auto");
		QString cropCenter = QLatin1String("center");
	} option;

	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addPositionalArgument(QLatin1String("<first image>"),
			tr(
				"Path to first image file. "
				"File name without extension must end with numeric value, for example: "
				"myimage_0005.png. Conversion will iterate from this image to next image in "
				"sequence until it exists."));
	parser.addOption(QCommandLineOption(option.mode,
			tr("Operating mode, default: video"), QLatin1String("<video|crop>")));
	parser.addOption(QCommandLineOption(option.reverse,
			tr("Output images in reverse order, useful to produce reversed videos.")));
	parser.addOption(QCommandLineOption(option.analyzeOnly,
			tr(
				"Analyze source images only. Does not trigger conversion. "
				"Useful to extract crop from the video")));
	parser.addOption(QCommandLineOption(option.crop,
			tr(
				"Crop mode, one of <none|auto|center|RECTANGLE>, where:\n"
				"  none           - do not crop at all, output images\n"
				"                   will have same dimension as source.\n"
				"  auto (default) - auto crop source images by\n"
				"                   eliminating completely transparent\n"
				"                   pixels.\n"
				"  center         - auto crop and enlarge crop rectangle\n"
				"                   to have the equal horizontal and\n"
				"                   vertical margins.\n"
				"  RECTANGLE      - manually provide exact crop rectangle\n"
				"                   in format 'x,y,w,h', example:\n"
				"                   --crop=10,12,20,30\n"
				"If crop rectangle is larger than source image then output image will have appropriate "
				"transparent border."),
			QLatin1String("mode")));
	parser.addOption(QCommandLineOption(option.cropAdjust,
			tr("Enlarge crop rect by this number of pixels."), QLatin1String("value")));
	parser.addOption(QCommandLineOption(option.scale,
			tr("Scale factor (0..1]."), QLatin1String("factor")));
	parser.addOption(QCommandLineOption(option.brightness,
			tr("Correct brightness: (0..100]"), QLatin1String("level")));
	parser.addOption(QCommandLineOption(option.fill,
			tr("Fill output image will <color>, usefull if only alpha channel needed."),
			QLatin1String("color")));
	parser.addOption(QCommandLineOption(option.alpha, QLatin1String("threshold"),
			tr("Pixel with alpha <= threshold considered transparent when detecting auto crop: [0..1]")));
	if (!parser.parse(QCoreApplication::arguments())) {
		throw ParserError(parser);
	}

	const QString modeString = parser.value(option.mode);
	if (!modeString.isEmpty()) {
		if (modeString == option.modeVideo) {
			mode_ = Mode::Video;
		} else if (modeString == option.modeCrop) {
			mode_ = Mode::Crop;
		} else {
			throw ParserError(parser, tr("Invalid mode"));
		}
	}

	reverse_ = parser.isSet(option.reverse);
	analyzeOnly_ = parser.isSet(option.analyzeOnly);

	const QString cropString = parser.value(option.crop);
	if (!cropString.isEmpty()) {
		if (cropString == option.cropAuto) {
			cropMode_ = CropMode::Auto;
		} else if (cropString == option.cropNone) {
			cropMode_ = CropMode::None;
		} else if (cropString == option.cropCenter) {
			centerAutoCrop_ = true;
		} else {
			const QStringList values = cropString.split(QLatin1Char(','));
			if (values.count() != 4) {
				throw ParserError(parser, tr("Invalid crop"));
			}

			bool ok;
			const int l = values.at(0).toInt(&ok);
			if (!ok) throw ParserError(parser, tr("Invalid crop left"));
			const int t = values.at(1).toInt(&ok);
			if (!ok) throw ParserError(parser, tr("Invalid crop top"));
			const int w = values.at(2).toInt(&ok);
			if (!ok || w <= 0) throw ParserError(parser, tr("Invalid crop width"));
			const int h = values.at(3).toInt(&ok);
			if (!ok || h <= 0) throw ParserError(parser, tr("Invalid crop height"));

			exactCrop_ = QRect(l, t, w, h);
			cropMode_ = CropMode::Exact;
		}
	}

	const QString cropAdjustString = parser.value(option.cropAdjust);
	if (!cropAdjustString.isEmpty()) {
		bool ok;
		cropAdjust_ = cropAdjustString.toInt(&ok);
		if (!ok || cropAdjust_ < 0) throw ParserError(parser, tr("Invalid crop-adjust"));
	}

	const QString scaleString = parser.value(option.scale);
	if (!scaleString.isEmpty()) {
		bool ok;
		scale_ = scaleString.toFloat(&ok);
		if (!ok || scale_ <= 0 || scale_ > 1.0f) throw ParserError(parser, tr("Invalid scale"));
	}

	const QString brightnessString = parser.value(option.brightness);
	if (!brightnessString.isEmpty()) {
		bool ok;
		brightness_ = brightnessString.toFloat(&ok);
		if (!ok || ok <= 0 || ok > 100) throw ParserError(parser, tr("Invalid brightness"));
	}

	const QString fillString = parser.value(option.fill);
	if (!fillString.isEmpty()) {
		fillColor_.setNamedColor(fillString);
		if (!fillColor_.isValid()) throw ParserError(parser, tr("Invalid fill color"));
	}

	const QString alphaString = parser.value(option.alpha);
	if (!alphaString.isEmpty()) {
		bool ok;
		alphaThreshold_ = alphaString.toFloat(&ok);
		if (!ok || alphaThreshold_ < 0 || (!qFuzzyCompare(alphaThreshold_, 1.0f)
				&& alphaThreshold_ > 1.0f)) {
			throw ParserError(parser, tr("Invalid alpha"));
		}
	}

	if (parser.positionalArguments().isEmpty()) {
		throw ParserError(parser);
	}

	const QString firstFile = parser.positionalArguments().at(0);

	const QFileInfo firstFileInfo(firstFile);
	const QDir dir = firstFileInfo.dir();

	const QString baseName = firstFileInfo.baseName();
	QString number;
	for (int i = baseName.length() - 1; i >= 0; i--) {
		const QChar ch = baseName.at(i);
		if (!ch.isDigit()) break;
		number.prepend(ch);
	}

	if (number.isEmpty()) {
		const QString message = tr("Invalid first file name, should end with number: %1")
				.arg(firstFileInfo.fileName());
		throw ParserError(parser, message);
	}

	const int numberLength = number.length();
	const QString namePrefix = baseName.left(baseName.length() - numberLength);

	QString suffix = firstFileInfo.completeSuffix();
	if (!suffix.isEmpty()) suffix.prepend(QLatin1Char('.'));

	const int first = number.toInt();

	inputPath_.reset(new Path(dir.filePath(namePrefix) + QLatin1String("%1") + suffix, numberLength, first));

	const QDir outputDir = QDir::current();
	outputPath_.reset(new Path(outputDir.filePath(namePrefix) + QLatin1String("%1.png"), numberLength, 0));
}


void App::_countFiles() noexcept
{
	int index = 0;
	while (true) {
		const QString filePath = inputPath_->pathForIndex(index);
		if (!QFile::exists(filePath)) break;
		index++;
	}

	fileCount_ = index;
}


void App::_start() noexcept
{
	_countFiles();

	if (fileCount_ == 0) {
		qWarning() << "No files found";
		QCoreApplication::exit(1);
		return;
	}

	if (cropMode_ == CropMode::Auto) {
		qInfo() << "Running analyzer...";
		analyzer_.reset(new Analyzer(threadPool_, *inputPath_, fileCount_, alphaThreshold_));
		connect(analyzer_.get(), &Analyzer::finished, this, &App::_analyzerFinished);
		analyzer_->start();
	} else {
		imageSize_ = Analyzer::findImageSize(*inputPath_);
		if (!imageSize_.isValid()) {
			qWarning() << "Failed to find image size";
			QCoreApplication::exit(1);
			return;
		}

		if (cropMode_ == CropMode::Exact) imageCrop_ = exactCrop_;

		_startConvertor();
	}
}


void App::_startConvertor() noexcept
{
	if (cropMode_ == CropMode::Auto) {
		imageCrop_ = imageCrop_.adjusted(-cropAdjust_, -cropAdjust_, cropAdjust_, cropAdjust_);

		if (centerAutoCrop_) {
			const int hmargin = std::min(imageCrop_.x(), imageSize_.width() - 1 - imageCrop_.right());
			const int vmargin = std::min(imageCrop_.y(), imageSize_.height() - 1 - imageCrop_.bottom());
			imageCrop_ = QRect(QPoint(hmargin, vmargin),
					QPoint(imageSize_.width() - 1 - hmargin, imageSize_.height() - 1 - vmargin));
		}
	}

	if (cropMode_ != CropMode::None) {
		// adjust crop height to even number of pixels, width will be multiplied by 2 anyway
		if ((imageCrop_.height() % 2) != 0) {
			imageCrop_.setBottom(imageCrop_.bottom() + 1);
		}
	}

	qInfo() << "Adjusted crop:" << imageCrop_;

	if (analyzeOnly_) {
		QCoreApplication::quit();
		return;
	}

	QSize scaleSize;
	if (scale_ != 0) {
		const QSize size = !imageCrop_.isNull() ? imageCrop_.size() : imageSize_;

		scaleSize = QSize(
			std::lround(std::ceil(size.width() * scale_)),
			std::lround(std::ceil(size.height() * scale_)));

		if ((scaleSize.width() % 2) != 0) scaleSize.rwidth()++;
		if ((scaleSize.height() % 2) != 0) scaleSize.rheight()++;
	}

	qInfo() << "Running convertion...";
	converter_.reset(new Converter(threadPool_, mode_, *inputPath_, *outputPath_, fileCount_, imageCrop_,
			reverse_, scaleSize, brightness_, fillColor_));
	connect(converter_.get(), &Converter::finished, this, &App::_converterFinished);
	converter_->start();
}


void App::_analyzerFinished(const bool ok) noexcept
{
	if (!ok) {
		qWarning() << "Analyze failed";
		QCoreApplication::exit(1);
		return;
	}

	(qInfo() << "Analyze finished: size=").nospace() << analyzer_->size()
			<< ", crop=" << analyzer_->crop();

	imageCrop_ = analyzer_->crop();
	imageSize_ = analyzer_->size();
	analyzer_.reset();

	_startConvertor();
}


void App::_converterFinished(const bool ok) noexcept
{
	if (!ok) {
		qWarning() << "Conversion failed";
		QCoreApplication::exit(1);
		return;
	}

	qInfo() << "Conversion finished";

	QCoreApplication::quit();
}
