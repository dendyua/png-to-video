
#include <QGuiApplication>

#include "App.h"




int main(int argc, char ** argv)
{
	QGuiApplication app(argc, argv);
	return App().exec();
}
