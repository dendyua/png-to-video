
#include "Converter.h"

#include <memory>
#include <cmath>

#include <QThreadPool>
#include <QPainter>
#include <QFile>
#include <QDebug>




void ConverterWorker::run() noexcept
{
	const QImage rawSource(options_.inputPath.pathForIndex(index_));
	if (rawSource.isNull()) {
		emit error();
		return;
	}

	const QImage unscaledSource = options_.crop.isNull() ? rawSource : rawSource.copy(options_.crop);

	const QImage source = options_.scaleSize.isValid() ? unscaledSource.scaled(options_.scaleSize,
			Qt::IgnoreAspectRatio, Qt::SmoothTransformation) : unscaledSource;

	if (options_.mode == Mode::Crop) {
		// output cropped source image
		if (!source.save(outputFilePath_, "png")) {
			emit error();
			return;
		}

		emit finished(index_);
		return;
	}

	const QSize imageSize = source.size();

	QImage alpha(source.size(), QImage::Format_RGB32);
	if (options_.brightness == 0) {
		for (int y = 0; y < imageSize.height(); ++y) {
			const QRgb * const s = reinterpret_cast<const QRgb*>(source.scanLine(y));
			QRgb * const d = reinterpret_cast<QRgb*>(alpha.scanLine(y));
			for (int x = 0; x < imageSize.width(); ++x) {
				const int a = qAlpha(s[x]);
				d[x] = qRgb(a, a, a);
			}
		}
	} else {
		const float exp = 1.0f / options_.brightness;
		for (int y = 0; y < imageSize.height(); ++y) {
			const QRgb * const s = reinterpret_cast<const QRgb*>(source.scanLine(y));
			QRgb * const d = reinterpret_cast<QRgb*>(alpha.scanLine(y));
			for (int x = 0; x < imageSize.width(); ++x) {
				const float sa = static_cast<float>(qAlpha(s[x])) / 255.0f;
				const int a = std::lround(std::pow(sa, exp) * 255.0f);
				d[x] = qRgb(a, a, a);
			}
		}
	}

	QImage color = QImage(imageSize, QImage::Format_RGB32);

	if (options_.fillColor.isValid()) {
		color.fill(options_.fillColor);
	} else {
		color.fill(Qt::black);
		QPainter pc(&color);
		pc.setCompositionMode(QPainter::CompositionMode_SourceIn);
		pc.drawImage(QPoint(), source);
		pc.end();

		if (options_.brightness != 0) {
			const int level = std::lround(options_.brightness * 100);
			for (int y = 0; y < imageSize.height(); ++y) {
				QRgb * const p = reinterpret_cast<QRgb*>(color.scanLine(y));
				for (int x = 0; x < imageSize.width(); ++x) {
					*p = QColor(*p).lighter(level).rgb();
				}
			}
		}
	}

	QImage target(source.width() * 2, source.height(), QImage::Format_RGB32);
	target.fill(0);
	QPainter p(&target);
	p.setCompositionMode(QPainter::CompositionMode_Source);
	p.drawImage(QPoint(), color);
	p.drawImage(QPoint(source.width(), 0), alpha);
	p.end();

	if (!target.save(outputFilePath_, "png")) {
		emit error();
		return;
	}

	emit finished(index_);
}




Converter::Converter(QThreadPool & pool, const Mode mode, const Path & inputPath,
		const Path & outputPath, int count, const QRect & crop, const bool reverse,
		const QSize & scaleSize, const float brightness, const QColor & fillColor) noexcept :
	pool_(pool),
	count_(count),
	reverse_(reverse),
	options_{mode, inputPath, outputPath, crop, scaleSize, brightness, fillColor}
{
}


void Converter::start() noexcept
{
	while (pool_.activeThreadCount() < pool_.maxThreadCount()) {
		if (!_tryNext()) {
			_checkDone();
			break;
		}
	}
}


bool Converter::_tryNext() noexcept
{
	if (currentIndex_ == count_) return false;

	const int index = currentIndex_;
	currentIndex_++;

	const QString outputFilePath = options_.outputPath.pathForIndex(!reverse_ ?
			index : count_ - index - 1);

	std::unique_ptr<ConverterWorker> worker(new ConverterWorker(index, options_,
			outputFilePath));

	connect(worker.get(), &QObject::destroyed, this, [this]() {
		workerCount_--;
		_checkDone();
	});

	connect(worker.get(), &ConverterWorker::error, this, [this]() {
		hasError_ = true;
	}, Qt::QueuedConnection);

	connect(worker.get(), &ConverterWorker::finished, this, [this](const int index) {
		qInfo() << "converted:" << index << "of" << count_;
		if (!hasError_) _tryNext();
	});

	workerCount_++;
	pool_.start(worker.release());
	return true;
}


void Converter::_checkDone() noexcept
{
	if (workerCount_ != 0) return;
	emit finished(!hasError_);
}
